<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'asilia');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7j)Mj)ODHnE[PV9 WMp|axd;kO(1t0uz;?r?Y]|Zi=K^j*n/mTb#brWK{pOLR3|8');
define('SECURE_AUTH_KEY',  'Bi/ied!~(Jm}[p)cTmZuC)Ln!fi<~=a#BnAND&;%VZqQLA wf4qJ(V,B)j qroIz');
define('LOGGED_IN_KEY',    'ajys=S+wm[NnA5|~$ h!Xk/d<Vrp9{6MF96qvWpY jb#nV@P>cU|I4>8$WRac_My');
define('NONCE_KEY',        'l-1xY2I7s;xC88}l#/n;??sDah/U&UU.We~9:IZJz}+%cE@_Q+`nn(XFpcPuKSF^');
define('AUTH_SALT',        'zO]rR86hP$GI&]>YM`)8-Fj^jdu7U)-a6w9CW)#1/:O.u-}TJ5HJ_Z>meUG7Z~Qu');
define('SECURE_AUTH_SALT', 'O):4UHHtps%]Ut[R%;B^RoS#jC>a/9k2k40j:` qY0Sja+G(T.8 q?-l=#gC%H!2');
define('LOGGED_IN_SALT',   'Z7?$>Jn5~LJ@)%g)E0Z=K]c/U,F]!t@J+to/!0JJn7D:gGT:D$h;@vJs:nnF~Y~L');
define('NONCE_SALT',       ';I)Tp+Pi%Yc(Owvel8KQ@=JrQ^cQ B- s>#!QRS_Ha{P@vC,P}c&.Wab<_Ys],LJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
