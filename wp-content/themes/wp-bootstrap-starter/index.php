<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
<!-- add the breadcrumbs here, will be static for now, but dynamic when  -->
<div class="container">
 <div id="breadcrumbs">
 <a href="#" class="home"></a> <a href="#">Where to Go</a> <a href="#">Kenya</a> <a href="#">Greater Masai Mara Naboisho Camp</a>
 </div>
	<section id="top-news" class="content-area col-sm-12 col-md-12 col-lg-12">
		<main id="main" class="site-main" role="main">
			<div class="col-lg-8">
			<?php
				$post_id = 30;
				$queried_post = get_post($post_id);
				$title = $queried_post->post_title;
				?>
				<h1><?php echo $queried_post->post_title; ?></h1>
				<?php echo $queried_post->post_content; ?>
			</div>
			<div class="col-lg-4">
			<img src="http://www.asiliaafrica.com/wp-content/uploads/2015/11/ken-naboisho-map-307x253.jpg">
			<br>GPS: 01°24’02” S, 035°18’13” E
			<br>
			<!-- trip advisor-->
			<div class="wpb_wrapper">
			<div id="TA_cdsscrollingravenarrow680" class="TA_cdsscrollingravenarrow" target="_blank"><div id="CDSSCROLLINGRAVE" class="gray    narrow "> <div class="rightBorder"> <a target="_blank" href="https://www.tripadvisor.co.za/"><img src="https://static.tacdn.com/img2/t4b/Stacked_TA_logo.png" alt="TripAdvisor" class="widEXCIMG" id="CDSWIDEXCLOGO"></a> </div> <marquee behavior="scroll" scrollamount="3" direction="left" onmouseover="this.stop();" onmouseout="this.start();"> <div> <span> <span class="reviewTitle">"Fantastic classic safari camp"</span> <span class="reviewContribution">29 September 2017 - A TripAdvisor Traveller</span> <span class="link">Read 512 reviews of <a target="_blank" href="https://www.tripadvisor.co.za/Hotel_Review-g294209-d2027329-Reviews-Naboisho_Camp_Asilia_Africa-Maasai_Mara_National_Reserve_Rift_Valley_Province.html" onclick="ta.cds.handleTALink($cdsConfig.getMcid()); return true;" rel="nofollow">Naboisho Camp, Asilia Africa</a></span> <div class="placeHolder"> &nbsp;</div> </span> </div> </marquee> <img src="https://www.tripadvisor.co.za/img/cdsi/partner/transparent_pixel-17198-2.gif" height="1" width="1"> </div> </div><script src="https://www.jscache.com/wejs?wtype=cdsscrollingravenarrow&amp;uniq=680&amp;locationId=2027329&amp;lang=en_ZA&amp;border=false&amp;backgroundColor=gray&amp;display_version=2"></script><script src="https://www.tripadvisor.co.za/WidgetEmbed-cdsscrollingravenarrow?border=false&amp;backgroundColor=gray&amp;locationId=2027329&amp;display_version=2&amp;uniq=680&amp;lang=en_ZA"></script>
		<script type="text/javascript">
		 jQuery("body").on("click","#CDSSCROLLINGRAVE a",function(e){
		 	e.preventDefault();
		 	window.location.href = "https://www.tripadvisor.co.za/Hotel_Review-g294209-d2027329-Reviews-Naboisho_Camp_Asilia_Africa-Maasai_Mara_National_Reserve_Rift_Valley_Province.html";
		 });
		</script>
		<!-- end trip advisor-->
		</div>
		</div>
		</div>
		</div>
		</div>
		</div>
		<div class="grey">
				<div class="container">
				<div class="div-left">
				<?php
				$post_id = 32;
				$queried_post = get_post($post_id);
				echo $queried_post->post_content;
				?>
				</div>
				</div>
		</div>

		<div class="orange">
				<div class="container">
				<div class="div-left">
				<?php
				$post_id = 42;
				$queried_post = get_post($post_id);
				echo $queried_post->post_content;
				?>
				</div>
				</div>
		</div>
		<!--awards -->
		<div class="awards">
				<div class="container">
				<div class="div-left">
				<?php
				$post_id = 44;
				$queried_post = get_post($post_id);
				echo $queried_post->post_content;
				?>
				</div>
				</div>
		</div>

		<!-- accomodation and tabs -->
		<div class="accomodation-section">
		<div class="container">
			<?php echo do_shortcode("[TABS_R id=49]"); ?>
		</div>
		</div>

		<!-- experiences -->
		<div class="experiences-activities">
		<div class="container">
			<?php
				$post_id = 56;
				$queried_post = get_post($post_id);
				echo $queried_post->post_content;
				?>
		</div>
		</div>
		<!-- instagram feed-->
		<!-- experiences -->
		<div class="insta-feed">
		<div class="container">
			<?php echo do_shortcode("[instagram-feed]"); ?>
		</div>
		</div>

<!-- experiences -->
		<div class="signup">
		<div class="container">
			<?php
				$post_id = 91;
				$queried_post = get_post($post_id);
				echo $queried_post->post_content;
				?>
		</div>
		</div>
		</main><!-- #main -->
	</section><!-- #primary -->
<?php
get_sidebar();
get_footer();
