<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
    <?php get_template_part( 'footer-widget' ); ?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
				<div class="row">
             <div class="col-sm-3">
             <h4>About Us</h4>
             	<ul>
             		<li>About Asilia</li>
             	</ul>
             </div>
             <div class="col-sm-3">
             	<h4>Regions</h4>
             	<ul>
             		<li>Kenya</li>
             	</ul>
             </div>
             <div class="col-sm-3">
             <h4>Get In touch</h4>
             	<ul>
             		<li>About Asilia</li>
             	</ul></div>
             <div class="col-sm-3">
             		<h4>Follow Asilia</h4>
             	<ul>
             		<li>About Asilia</li>
             	</ul>
             </div>
             	</div>
            </div><!-- close .site-info -->
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
